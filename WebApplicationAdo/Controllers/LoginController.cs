﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplicationAdo.Repositorio;

namespace WebApplicationAdo.Controllers
{
    public class LoginController : Controller
    {
        TimeRepositorio _repositorio;

        public LoginController()
        {
            _repositorio = new TimeRepositorio();
        }

        //// GET: Login
        public ActionResult Index()
        {
            return View();
        }

        //// POST: Login
        [HttpPost]

        public ActionResult Entrar(FormCollection collection)
        {
            var usuarioLogado = _repositorio.ObterTimes(collection.Get("email"), collection.Get("senha"));
            if (usuarioLogado != null)
            {
                Session["usuarioLogado"] = usuarioLogado;
                return Redirect("/");
            }

            TempData["erro"] = "Verifique seu E-mail e Senha.";
            return Redirect("/Login");
        }
    }
}
